package com.achjqz.api

import java.nio.charset.StandardCharsets.US_ASCII
import java.nio.charset.StandardCharsets.UTF_8
import java.security.MessageDigest
import java.security.SecureRandom
import java.util.*
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

object Aes256 {
    /**
     * OpenSSL"s magic initial bytes.
     */
    private const val SALTED_STR = "Salted__"
    private val SALTED_MAGIC = SALTED_STR.toByteArray(US_ASCII)


    /**
     * Encrypt the text
     *
     * @param text The data to encrypt
     * @param password  The password / key to encrypt with.
     * @return A base64 encoded string containing the encrypted data.
     * @throws Exception
     */
    @Throws(Exception::class)
    fun encrypt(text: String, password: String): String {
        val pass = password.toByteArray(US_ASCII)
        val salt = SecureRandom().generateSeed(8)
        val inBytes = text.toByteArray(UTF_8)

        val passAndSalt = array_concat(pass, salt)
        var hash = ByteArray(0)
        var keyAndIv = ByteArray(0)
        var i = 0
        while (i < 3 && keyAndIv.size < 48) {
            val hashData = array_concat(hash, passAndSalt)
            val md = MessageDigest.getInstance("MD5")
            hash = md.digest(hashData)
            keyAndIv = array_concat(keyAndIv, hash)
            i++
        }

        val keyValue = Arrays.copyOfRange(keyAndIv, 0, 32)
        val iv = Arrays.copyOfRange(keyAndIv, 32, 48)
        val key = SecretKeySpec(keyValue, "AES")

        val cipher = Cipher.getInstance("AES/CBC/PKCS5Padding")
        cipher.init(Cipher.ENCRYPT_MODE, key, IvParameterSpec(iv))
        var data = cipher.doFinal(inBytes)
        data = array_concat(array_concat(SALTED_MAGIC, salt), data)
        return Base64.getEncoder().encodeToString(data)
    }

    private fun array_concat(a: ByteArray, b: ByteArray): ByteArray {
        val c = ByteArray(a.size + b.size)
        System.arraycopy(a, 0, c, 0, a.size)
        System.arraycopy(b, 0, c, a.size, b.size)
        return c
    }
}