package com.achjqz.api.bilibili

import kotlin.math.pow

object BVConverter {
    private val table = "fZodR9XQDSUm21yCkr6zBqiveYah8bt4xsWpHnJE7jL5VG3guMTKNPAwcF"
    private val s = listOf(11, 10, 3, 8, 4, 6)
    private val xor = 177451812L
    private val add = 8728348608
    private val tr = hashMapOf<Char, Int>()

    init {
        for (i in 0 until 58) {
            tr[table[i]] = i
        }
    }

    private fun get58Pow(i: Int): Long = 58.toDouble().pow(i).toLong()
    fun dec(x: String): Long {
        var r = 0L
        for (i in 0 until 6) {
            r += tr[x[s[i]]]!!.times(get58Pow(i))
        }
        return (r - add).xor(xor)
    }

    fun enc(x: Long): String {
        val mav = x.xor(xor) + add
        val r = "BV1  4 1 7  ".toCharArray()
        for (i in 0 until 6) {
            r[s[i]] = table[(mav / get58Pow(i) % 58).toInt()]
        }
        return String(r)
    }
}