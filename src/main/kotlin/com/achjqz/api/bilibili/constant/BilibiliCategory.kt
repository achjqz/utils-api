package com.achjqz.api.bilibili.constant

enum class BilibiliCategory(val id: String) {
    // 番剧
    BANGUMI("1"),

    // 电影
    MOVIE("2"),

    // 纪录片
    DOCUMENTARY("3"),

    // 国创
    CHINA("4"),

    // 电视剧
    TV("5")
}
