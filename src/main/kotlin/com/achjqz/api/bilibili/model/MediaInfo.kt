package com.achjqz.api.bilibili.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MediaInfo(
        val code: String,
        val message: String,
        val result: Result? = null
) {
    @JsonClass(generateAdapter = true)
    data class Result(
            val data: List<Data>,
            val page: Page
    ) {
        @JsonClass(generateAdapter = true)
        data class Data(
                val cover: String,
                @Json(name = "index_show")
                val indexShow: String = "",
                @Json(name = "is_finish")
                val isFinish: Int,
                val link: String,
                val title: String,
                @Json(name = "season_id")
                val seasonId: Long
        )

        @JsonClass(generateAdapter = true)
        data class Page(
                val num: Int,
                val size: Int,
                val total: Int
        )
    }
}