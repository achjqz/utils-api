package com.achjqz.api.bilibili.model


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class NormalVideoInfo(
        @Json(name = "code")
        val code: Int = 0,
        @Json(name = "data")
        val `data`: Data = Data(),
        @Json(name = "message")
        val message: String = ""
) {
    @JsonClass(generateAdapter = true)
    data class Data(
            @Json(name = "aid")
            val aid: Long = 0,
            @Json(name = "attribute")
            val attribute: Int = 0,
            @Json(name = "cid")
            val cid: Long = 0,
            @Json(name = "ctime")
            val ctime: Int = 0,
            @Json(name = "desc")
            val desc: String = "",
            @Json(name = "duration")
            val duration: Int = 0,
            @Json(name = "dynamic")
            val `dynamic`: String = "",
            @Json(name = "owner")
            val owner: Owner = Owner(),
            @Json(name = "pages")
            val pages: List<Page> = listOf(),
            @Json(name = "pic")
            val pic: String = "",
            @Json(name = "pubdate")
            val pubdate: Int = 0,
            @Json(name = "share_subtitle")
            val shareSubtitle: String = "",
            @Json(name = "short_link")
            val shortLink: String = "",
            @Json(name = "stat")
            val stat: Stat = Stat(),
            @Json(name = "tag")
            val tag: List<Tag> = listOf(),
            @Json(name = "title")
            val title: String = "",
            @Json(name = "tname")
            val tname: String = ""
    ) {

        @JsonClass(generateAdapter = true)
        data class Owner(
                @Json(name = "face")
                val face: String = "",
                @Json(name = "mid")
                val mid: Int = 0,
                @Json(name = "name")
                val name: String = ""
        )


        @JsonClass(generateAdapter = true)
        data class Page(
                @Json(name = "cid")
                val cid: Long = 0,
                @Json(name = "dmlink")
                val dmlink: String = "",
                @Json(name = "duration")
                val duration: Int = 0,
                @Json(name = "page")
                val page: Int = 0,
                @Json(name = "part")
                val part: String = ""
        ) {

            @JsonClass(generateAdapter = true)
            data class Owner(
                    @Json(name = "face")
                    val face: String = "",
                    @Json(name = "mid")
                    val mid: Int = 0,
                    @Json(name = "name")
                    val name: String = ""
            )
        }

        @JsonClass(generateAdapter = true)
        data class Stat(
                @Json(name = "aid")
                val aid: Int = 0,
                @Json(name = "coin")
                val coin: Int = 0,
                @Json(name = "danmaku")
                val danmaku: Int = 0,
                @Json(name = "dislike")
                val dislike: Int = 0,
                @Json(name = "favorite")
                val favorite: Int = 0,
                @Json(name = "his_rank")
                val hisRank: Int = 0,
                @Json(name = "like")
                val like: Int = 0,
                @Json(name = "now_rank")
                val nowRank: Int = 0,
                @Json(name = "reply")
                val reply: Int = 0,
                @Json(name = "share")
                val share: Int = 0,
                @Json(name = "view")
                val view: Int = 0
        )

        @JsonClass(generateAdapter = true)
        data class Tag(
                @Json(name = "attribute")
                val attribute: Int = 0,
                @Json(name = "cover")
                val cover: String = "",
                @Json(name = "hated")
                val hated: Int = 0,
                @Json(name = "hates")
                val hates: Int = 0,
                @Json(name = "is_activity")
                val isActivity: Int = 0,
                @Json(name = "liked")
                val liked: Int = 0,
                @Json(name = "likes")
                val likes: Int = 0,
                @Json(name = "tag_id")
                val tagId: Int = 0,
                @Json(name = "tag_name")
                val tagName: String = "",
                @Json(name = "tag_type")
                val tagType: String = "",
                @Json(name = "uri")
                val uri: String = ""
        )
    }
}