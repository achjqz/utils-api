package com.achjqz.api.bilibili.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RecommendInfo(
        val code: Int, // 0
        val message: String, // success
        val result: Result? = null
) {
    @JsonClass(generateAdapter = true)
    data class Result(
            val modules: List<Module>
    ) {
        @JsonClass(generateAdapter = true)
        data class Module(
                var items: List<Item>,
                var size: Int, // 10
                var style: String = "", // fall
                var title: String = "" // 编辑推荐
        ) {
            @JsonClass(generateAdapter = true)
            data class Item(
                    var cover: String = "", // http://i0.hdslb.com/bfs/bangumi/57e00f9995459ab0cf40800358ee7f3b392b38a4.jpg
                    var link: String = "", // https://www.bilibili.com/blackboard/topic/activity-dm4qK4-BI.html
                    var title: String = "" // 【资讯档】2019年第7周
            )
        }
    }
}
