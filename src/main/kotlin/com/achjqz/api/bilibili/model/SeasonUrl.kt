package com.achjqz.api.bilibili.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SeasonUrl(
        val code: String = "",
        val durl: List<Durl>? = null
) {
    @JsonClass(generateAdapter = true)
    data class Durl(
            val size: Long,
            val length: Long,
            val url: String,
            val order: Int
    )
}