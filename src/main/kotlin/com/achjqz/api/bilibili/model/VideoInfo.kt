package com.achjqz.api.bilibili.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class VideoInfo(
        val code: Int,
        val data: Data? = null
) {
    @JsonClass(generateAdapter = true)
    data class Data(
            val bvid: String
    )
}