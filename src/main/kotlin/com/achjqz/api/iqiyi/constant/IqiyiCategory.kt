package com.achjqz.api.iqiyi.constant

enum class IqiyiCategory(val id: String) {
    MOVIE("1"),
    TV("2"),
    DOCUMENTARY("3"),
    ANIME("4"),
    SHOW("6"),
    CHILD("15")
}