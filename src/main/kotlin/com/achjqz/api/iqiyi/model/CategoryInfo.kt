package com.achjqz.api.iqiyi.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CategoryInfo(
        val msg: String,
        val total: Int,
        val code: String,
        val data: List<Data>
) {
    @JsonClass(generateAdapter = true)
    data class Data(
            val initIssueTime: String = "",
            val name: String,
            val pic: String,
            val qpId: Long,
            val tvQId: Long,
            val score: Double = 8.0,
            val shortName: String = "",
            val tag: String = ""
    )
}