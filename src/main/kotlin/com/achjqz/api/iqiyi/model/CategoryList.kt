package com.achjqz.api.iqiyi.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CategoryList(
        val msg: String,
        val code: String,
        val total: Int,
        val data: List<Data>

) {
    @JsonClass(generateAdapter = true)
    data class Data(
            val icon: String,
            val name: String,
            val id: Int
    )
}