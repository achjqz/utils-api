package com.achjqz.api.iqiyi.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SearchResult(
        val code: String,
        val data: Data
) {
    @JsonClass(generateAdapter = true)
    data class Data(
            val docinfos: List<DocInfo>,
            @Json(name = "max_result_number")
            val max: Int,
            @Json(name = "real_query")
            val query: String
    ) {
        @JsonClass(generateAdapter = true)
        data class DocInfo(
                val score: Double,
                val albumDocInfo: AlbumDocInfo
        ) {
            @JsonClass(generateAdapter = true)
            data class AlbumDocInfo(
                    val albumId: Long = -1L,
                    val albumTitle: String = "",
                    val channel: String = "",
                    val albumVImage: String = "",
                    val albumHImage: String = "",
                    val director: String = "",
                    val star: String = "",
                    val region: String = "",
                    val releaseDate: String = "",
                    val score: Double = 8.0,
                    val playCount: Long = 0
            )
        }

    }
}