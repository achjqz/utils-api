package com.achjqz.api.iqiyi.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class VideoIDInfo(
        val code: Int, // 0
        @Json(name = "play_aid")
        val aid: Long, // 4000724600
        @Json(name = "play_tvid")
        val tvid: Long // 4000724600
)