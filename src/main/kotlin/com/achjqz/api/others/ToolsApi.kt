package com.achjqz.api.others

import com.achjqz.api.*
import com.achjqz.api.others.model.GarbageInfo
import com.achjqz.api.others.model.PornhubVideoUrl
import com.achjqz.api.others.model.ZhiHuVideoUrl
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody

object ToolsApi {
    private const val ZHIHU_VIDEO = "/zhihu_video_downloader"
    private const val PORNHUB_VIDEO = "/pornhub_video_downloader"
    private const val GARBAGE_CLASS = "/garbage_class"
    private val json = "application/json; charset=utf-8".toMediaType()

    private fun getRequest(path: String, data: String): Request {
        val time = getCurrentSecond().toString()
        val sign = "$time$path$SECRET_KEY".sha256() + "." + Aes256.encrypt(time, SECRET_KEY)
        return Request.Builder().url(BASE_URL + path)
                .addHeader("Origin", ORIGIN_URL)
                .addHeader("Authsign", sign).post(data.toRequestBody(json)).build()
    }

    fun zhihuVideoUrl(url: String): ZhiHuVideoUrl? {
        client.newCall(getRequest(ZHIHU_VIDEO, "{\"url\": \"$url\"}")).execute().use {
            val body = it.body!!.string()
            val adapter = moshi.adapter<ZhiHuVideoUrl>(ZhiHuVideoUrl::class.java)
            return adapter.fromJson(body)
        }
    }

    fun pornhubVideoUrl(url: String): PornhubVideoUrl? {
        client.newCall(getRequest(PORNHUB_VIDEO, "{\"url\": \"$url\"}")).execute().use {
            val body = it.body!!.string()
            val adapter = moshi.adapter<PornhubVideoUrl>(PornhubVideoUrl::class.java)
            return adapter.fromJson(body)
        }
    }

    fun garbageQuery(key: String): GarbageInfo? {
        client.newCall(getRequest(GARBAGE_CLASS, "{\"k\": \"$key\"}")).execute().use {
            val body = it.body!!.string()
            val adapter = moshi.adapter<GarbageInfo>(GarbageInfo::class.java)
            return adapter.fromJson(body)
        }
    }


}