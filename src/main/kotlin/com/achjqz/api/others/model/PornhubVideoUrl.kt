package com.achjqz.api.others.model


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PornhubVideoUrl(
        @Json(name = "data")
        val `data`: List<Data> = listOf(),
        @Json(name = "msg")
        val msg: String = ""
) {
    @JsonClass(generateAdapter = true)
    data class Data(
            @Json(name = "defaultQuality")
            val defaultQuality: Boolean = false,
            @Json(name = "format")
            val format: String = "",
            @Json(name = "quality")
            val quality: String = "",
            @Json(name = "videoUrl")
            val videoUrl: String = ""
    )
}