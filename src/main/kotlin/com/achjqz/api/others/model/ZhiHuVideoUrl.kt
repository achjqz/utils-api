package com.achjqz.api.others.model


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ZhiHuVideoUrl(
        @Json(name = "data")
        val `data`: List<String> = listOf(),
        @Json(name = "msg")
        val msg: String = ""
)