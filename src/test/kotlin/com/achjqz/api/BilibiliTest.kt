package com.achjqz.api

import com.achjqz.api.bilibili.BVConverter
import com.achjqz.api.bilibili.BilibiliApi
import com.achjqz.api.bilibili.constant.BilibiliCategory
import com.achjqz.api.bilibili.constant.BilibiliSearchType
import org.junit.Test

class BilibiliTest {

    @Test
    fun testBVConverter() {
        println(BVConverter.dec("BV1h7411o7bV"))
        println(BVConverter.enc(170001))
//        for (i in 0..500) {
//            val n = (100..99999999).random()
//            val url = "https://api.bilibili.com/x/web-interface/archive/stat?aid=$n"
//            client.newCall(url.toSimpleRequest()).execute().use {
//                val body = it.body?.string()
//                val adapter = moshi.adapter<VideoInfo>(VideoInfo::class.java)
//                val data = adapter.fromJson(body!!)
//                if(data?.code == 0) {
//                    assert(data.data!!.bvid == BVConverter.enc(n.toLong()))
//                }
//            }
//        }

    }

    @Test
    fun enum() {
        println(BilibiliCategory.BANGUMI.id)
    }

    @Test
    fun main_api() {
        println(BilibiliApi.getTypeSearchResult(keyword = "四月"))
        println(BilibiliApi.getRecommendInfo())
        println(BilibiliApi.getMediaInfo(BilibiliCategory.BANGUMI))
        println(BilibiliApi.getSeasonInfo(1699))
        println(BilibiliApi.getSeasonPlayUrlInfo(1608327, 3386418))
    }

    @Test
    fun season_info() {
        println(BilibiliApi.getSeasonInfo(24053))
    }

    @Test
    fun media_info() {
        println(BilibiliApi.getMediaInfo(BilibiliCategory.CHINA))
    }

    @Test
    fun vip_movie() {
        println(BilibiliApi.getSeasonPlayUrlInfo(40961594, 88446766))
    }

    @Test
    fun type_search() {
        println(BilibiliApi.getTypeSearchResult(BilibiliSearchType.VIDEO, "哈利波特", 1, 24))
    }

    @Test
    fun normal() {
        val aid = "95851278"
        val res = BilibiliApi.getNormalVideoInfo(aid)
        println(res?.data?.pages)
        for (page in res!!.data.pages) {
            println(BilibiliApi.getNormalVideoPlayUrl(res.data.aid, page.cid.toString()))
        }

    }

    @Test
    fun ep() {
        println(BilibiliApi.getEpVideoInfo(259769))
    }
}