package com.achjqz.api

import com.achjqz.api.iqiyi.IqiyiApi
import com.achjqz.api.iqiyi.constant.IqiyiCategory
import org.junit.Test

class IqiyiTest {
    @Test
    fun getTvid() {
        println(IqiyiApi.getTvidFromH5Url("https://www.iqiyi.com/v_19rrqn12c4.html"))
    }

    @Test
    fun getDetail() {
        println(IqiyiApi.getDetailInfo(167419))
    }

    @Test
    fun category() {
        //println(IqiyiApi.getCategoryList())
        println(IqiyiApi.getCategoryInfo(IqiyiCategory.ANIME, 1, 20))
    }

    @Test
    fun episode() {
        println(IqiyiApi.getEpisodeInfo(4000724600))
    }

    @Test
    fun url() {
        val params = IqiyiApi.getPlayUrlParam(723851500, "b9e57c254a58d3213263f78f476facf5")
        IqiyiApi.getPlayUrl(params, "123")
    }

    @Test
    fun search() {
        println(IqiyiApi.globalSearch("名侦探", 9, 24))
    }


}